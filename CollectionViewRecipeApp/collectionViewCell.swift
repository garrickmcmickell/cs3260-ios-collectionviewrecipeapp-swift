//
//  collectionViewCell.swift
//  CollectionViewRecipeApp
//
//  Created by Garrick McMickell on 10/20/16.
//  Copyright © 2016 Garrick McMickell. All rights reserved.
//

import UIKit

class collectionViewCell: UICollectionViewCell {
    @IBOutlet var label: UILabel!
    @IBOutlet var imageView: UIImageView!
}
