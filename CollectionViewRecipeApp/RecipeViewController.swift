//
//  RecipeViewController.swift
//  CollectionViewRecipeApp
//
//  Created by Garrick McMickell on 10/20/16.
//  Copyright © 2016 Garrick McMickell. All rights reserved.
//

import UIKit

class RecipeViewController: UIViewController {

    @IBOutlet var label: UILabel!
    @IBOutlet var textView: UITextView!
    @IBOutlet var textView1: UITextView!
    @IBOutlet var imageView: UIImageView!
    var dictionary = [String: String]()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        label.text = dictionary["title"]
        textView.text = dictionary["ingredients"]
        textView1.text = dictionary["directions"]
        
        let url = NSURL(string: dictionary["image"]!)
        let data = try? Data(contentsOf: url as! URL)
        imageView.image = UIImage(data: data!)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
