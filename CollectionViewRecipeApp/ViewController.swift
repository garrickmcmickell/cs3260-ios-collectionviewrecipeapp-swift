//
//  ViewController.swift
//  CollectionViewRecipeApp
//
//  Created by Garrick McMickell on 10/20/16.
//  Copyright © 2016 Garrick McMickell. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    @IBOutlet var collectionView: UICollectionView!
    
    var dictionaries = [[String: String]]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let recipe1: [String: String] = ["title": "Easy Garlic Chicken", "ingredients": "1 1/2 pounds skinless, boneless chicken breast halves \n1/4 cup butter \n6 cloves crushed garlic \n2 cups seasoned dry bread crumbs", "directions": "1. Preheat oven to 375 degrees F (190 degrees C). \n 2. In a small saucepan melt butter/margarine with garlic. Dip chicken pieces in butter/garlic sauce, letting extra drip off, then coat completely with bread crumbs. \n 3. Place coated chicken in a lightly greased 9x13 inch baking dish. Combine any leftover butter/garlic sauce with bread crumbs and spoon mixture over chicken pieces. Bake in the preheated oven for 45 minutes to 1 hour.", "image": "http://images.media-allrecipes.com/userphotos/250x250/00/25/80/258090.jpg"]
        dictionaries.append(recipe1)
        let recipe2: [String: String] = ["title": "Easy Tuna Casserole", "ingredients": "3 cups cooked macaroni \n1 (6 ounce) can tuna, drained \n1 (10.75 ounce) can condensed cream of chicken soup \n1 cup shredded Cheddar cheese \n1 1/2 cups French fried onions", "directions": "1. Preheat oven to 350 degrees F (175 degrees C). \n2. In a 9x13-inch baking dish, combine the macaroni, tuna, and soup. Mix well, and then top with cheese. \n3. Bake at 350 degrees F (175 degrees C) for about 25 minutes, or until bubbly. Sprinkle with fried onions, and bake for another 5 minutes. Serve hot.", "image": "http://images.media-allrecipes.com/userphotos/250x250/00/66/86/668635.jpg"]
        dictionaries.append(recipe2)
        let recipe3: [String: String] = ["title": "Easy Caramelized Onion Pork Chops", "ingredients": "1 tablespoon vegetable oil \n4 (4 ounce) pork loin chops, 1/2 inch thick \n3 teaspoons seasoning salt \n2 teaspoons ground black pepper \n1 onion, cut into strips \n1 cup water", "directions": "1. Rub chops with 2 teaspoons seasoning salt and 1 teaspoon pepper, or to taste. \n2. In a skillet, heat oil over medium heat. Brown pork chops on each side. Add the onions and water to the pan. Cover, reduce heat, and simmer for 20 minutes. \n3. Turn chops over, and add remaining salt and pepper. Cover, and cook until water evaporates and onions turn light to medium brown. Remove chops from pan, and serve with onions on top.", "image":"http://images.media-allrecipes.com/userphotos/250x250/01/07/50/1075072.jpg"]
        dictionaries.append(recipe3)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dictionaries.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! collectionViewCell
        
        let dictionary = dictionaries[indexPath.row]
        
        cell.label.text = dictionary["title"]
        
        let url = NSURL(string: dictionary["image"]!)
        let data = try? Data(contentsOf: url as! URL)
        cell.imageView.image = UIImage(data: data!)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        performSegue(withIdentifier: "moveToRecipes", sender: indexPath)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let rvc: RecipeViewController = segue.destination as! RecipeViewController
        let ip = sender as! NSIndexPath

        rvc.dictionary = dictionaries[ip.row]
    }
}

